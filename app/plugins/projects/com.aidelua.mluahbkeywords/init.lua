appname="更多关键字 (MLua手册)"--插件名
packagename="com.aidelua.mluahbkeywords"--插件包名
appver="1.0"
appcode=1099
mode="plugin"--模式：插件
description="本插件仅提供关键字提示，不提供相关函数"
utilversion="3.1"--Util版本，此变量不起作用
smallicon=false--当icon.png为小图标时，启用此项
supported2={--支持的APP
  aidelua={mincode=50499,targetcode=59999}
}
thirdplugins={}